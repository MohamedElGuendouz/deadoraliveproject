package com.company.model;

import java.util.Observable;

/**
 * Model of the grille in the application
 */
public class Grille extends Observable {
    /**
     *  Width of the grid
     */
    private int x;
    /**
     *  Height of the grid
     */
    private int y;
    /**
     *  the list containing the characters of the grid
     */
    private String[][] grille;


    /**
     * @return Width of the grid
     */
    public int getX() {
        return x;
    }

    /**
     *  define the width of the grid
     * @param x int for width of the grid
     */
    public void setX(int x) {
        this.x = x;
    }

    /**
     * @return Height of the grid
     */
    public int getY() {
        return y;
    }

    /**
     *  define the height of the grid
     *  @param y int for height of the grid
     */
    public void setY(int y) {
        this.y = y;
    }

    /**
     * @return the list of string in two dimensions
     */
    public String[][] getGrille() {
        return grille;
    }

    /**
     *  contructor of the model
     */
    public Grille() {
    }

    /**
     * Update the grid in the model
     */
    public void updateValue() {
        String[][] tmp = new String[x][y];
        for (int i = 0; i < this.x; i++) {
            for (int j = 0; j < this.y; j++) {
                // If the cell is a living cell with less than 2 or more than 3 neighbours
                if(this.grille[i][j] == "#" && (getNumbersOfNeighbouringCells(i, j) < 2 || getNumbersOfNeighbouringCells(i, j) > 3)) {
                    tmp[i][j] = ".";
                }
                // If the cell is a dead cell and has 3 neighbours
                if(this.grille[i][j] == "." && getNumbersOfNeighbouringCells(i, j) == 3) {
                    tmp[i][j] = "#";
                }
            }
        }

        updateGrille(tmp);
        setChanged();
        notifyObservers(grille);
    }

    /**
     * @param i position in witdh
     * @param j position in height
     * @return return the numbers of neighbours
     */
    private int getNumbersOfNeighbouringCells(int i, int j) {
        int count=0;

        if( i==0 && j == 0) {
            if(this.grille[i+1][j]=="#") {
                count++;
            }if(this.grille[i][j+1]=="#") {
                count++;
            }if(this.grille[i+1][j+1]=="#") {
                count++;
            }
        } else if (i == 0 && j == this.getY()-1) {
            if(this.grille[i][j-1]=="#") {
                count++;
            }if(this.grille[i+1][j]=="#") {
                count++;
            }if(this.grille[i+1][j-1]=="#") {
                count++;
            }
        } else if (i == this.getX()-1 && j == 0) {
            if(this.grille[i-1][j]=="#") {
                count++;
            }if(this.grille[i][j+1]=="#") {
                count++;
            }if(this.grille[i-1][j+1]=="#") {
                count++;
            }
        } else if (i == this.getX()-1 && j == this.getY()-1) {
            if(this.grille[i][j-1]=="#") {
                count++;
            }if(this.grille[i-1][j]=="#") {
                count++;
            }if(this.grille[i-1][j-1]=="#") {
                count++;
            }
        } else {
            if (i == 0) {
                if(this.grille[i+1][j]=="#") {
                    count++;
                }if(this.grille[i][j-1]=="#") {
                    count++;
                }if(this.grille[i][j+1]=="#") {
                    count++;
                }if(this.grille[i+1][j-1]=="#") {
                    count++;
                }if(this.grille[i+1][j+1]=="#") {
                    count++;
                }
            } else if (i == this.getX()-1) {
                if(this.grille[i-1][j]=="#") {
                    count++;
                }if(this.grille[i][j-1]=="#") {
                    count++;
                }if(this.grille[i][j+1]=="#") {
                    count++;
                }if(this.grille[i-1][j-1]=="#") {
                    count++;

                }if(this.grille[i-1][j+1]=="#") {
                    count++;
                }
            } else if (j == 0) {
                if(this.grille[i][j+1]=="#") {
                    count++;
                }if(this.grille[i-1][j]=="#") {
                    count++;
                }if(this.grille[i+1][j]=="#") {
                    count++;
                }if(this.grille[i-1][j+1]=="#") {
                    count++;
                }if(this.grille[i+1][j+1]=="#") {
                    count++;
                }
            } else if (j == this.getY()-1) {
                if(this.grille[i][j-1]=="#") {
                    count++;
                }if(this.grille[i-1][j]=="#") {
                    count++;
                }if(this.grille[i+1][j]=="#") {
                    count++;
                }if(this.grille[i-1][j-1]=="#") {
                    count++;
                }if(this.grille[i+1][j-1]=="#") {
                    count++;
                }
            } else {
                if (this.grille[i - 1][j] == "#") {
                    count++;
                }
                if (this.grille[i + 1][j] == "#") {
                    count++;
                }
                if (this.grille[i][j - 1] == "#") {
                    count++;
                }
                if (this.grille[i][j + 1] == "#") {
                    count++;
                }
                if (this.grille[i - 1][j - 1] == "#") {
                    count++;
                }
                if (this.grille[i + 1][j - 1] == "#") {
                    count++;
                }
                if (this.grille[i - 1][j + 1] == "#") {
                    count++;
                }
                if (this.grille[i + 1][j + 1] == "#") {
                    count++;
                }
            }
        }
        return count;
    }

    /**
     * @param x the width of grid
     * @param y the height of grid
     */
    public void initGrille(int x, int y) {
        this.x = x;
        this.y = y;
        this.grille = new String[x][y];
    }

    /**
     * @param grilleStart a grid for init application
     */
    public void setGrille(String[][] grilleStart) {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                grille[i][j] = grilleStart[i][j];
            }
        }
    }

    /**
     * @param grilleUpdate the grid with update
     */
    public void updateGrille(String[][] grilleUpdate) {
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                if(grilleUpdate[i][j] != null){
                    grille[i][j] = grilleUpdate[i][j];
                }
            }
        }
    }
}
