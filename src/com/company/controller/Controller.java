package com.company.controller;

import com.company.model.Grille;
import com.company.view.View;

/**
 * Controller of the application
 */
public class Controller {
    /**
     * The model of application
     */
    private final Grille model;

    /**
     * The view of application
     */
    private final View view;

    /**
     * @return The model of application
     */
    public Grille getModel() {
        return model;
    }

    /**
     * @return The view of application
     */
    public View getView() {
        return view;
    }


    /**
     * @param model model of the application
     * @param view view of the application
     * Contructor of the controller
     */
    public Controller(Grille model, View view) {
        this.model = model;
        this.view = view;
    }

    /**
     * Opreation for run the project
     */
    public void run() {
        for (int i = 0; i < 5; i++) {
            System.out.println("---------------------------------");
            System.out.println("Iteration n°"+(i+1));
            System.out.println("---------------------------------");
            model.updateValue();
            view.printGrille(this.getModel().getX(), this.getModel().getY(), this.getModel().getGrille());
        }
    }
}
