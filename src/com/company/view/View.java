package com.company.view;

import java.util.Observable;
import java.util.Observer;

/**
 * View of the application
 */
public class View implements Observer {
    /**
     *  constructor of the application
     */
    public View() {
    }


    /**
     * @param o Observable
     * @param arg Arguments
     *  Method for update this Observer
     */
    @Override
    public void update(Observable o, Object arg) {

    }

    /**
     * @param x width
     * @param y height
     * @param grille the grid
     *
     * Show the grid in command line
     */
    public void printGrille(int x, int y, String[][] grille) {
        for (int i = 0; i < x; i++) {
            System.out.print("|");
            for (int j = 0; j < y; j++) {
                System.out.print(grille[i][j]);
            }
            System.out.println("|");
        }
    }
}
