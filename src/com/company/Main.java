package com.company;

import com.company.controller.Controller;
import com.company.model.Grille;
import com.company.view.View;

/**
 * Main program of application
 */
public class Main {

    /**
     * @param args
     *
     * Main programm of this application
     */
    public static void main(String[] args) {

        String[][] grilleStart = {{"#","#",".",".","#",".",".",".","#","#"},
                {"#","#","#",".",".",".","#","#",".","."},
                {".",".","#",".",".","#",".",".","#","#"},
                {".","#",".",".","#",".","#","#",".","."}};


        View view = new View();
        Grille model = new Grille();
        model.initGrille(4,10);
        model.setGrille(grilleStart);
        model.addObserver(view);
        Controller controller = new Controller(model, view);
        controller.run();
    }
}
